# Recruiting Challenge 

### Running the App localy
```
git clone git@bitbucket.org:ighormartins/recruiting-challenge.git
npm install
npm start
```


## Development

* The APP is using Redux to manage it's states and Saga hor handling async stuff
* It uses react-router. And there is essentially 2 routes and 2 main features: Registering users and checking for past registered users
* There are four reducers:
1. Selected user (Used to display the greeting message for the current selected user)
2. Users (Users registered on the App)
3. Countries (Countries to be used on user registering)
4. Session Users (The users registered on the current session. Users are only available on the home page until refreshing. Then they'll be available at the revisited page protected by auth)
* The App is internationalized. 
* For styling, it's using MUICSS. However, MUICSS components are wrapped in custom components for easy maintenance / changes and minimum dependency  
* It's also using sass. 
* Users data are being stored in the localStorage through saga workers. (It's using a fake delay to mock an async API call for retrieving and storing users)
* There is a HOC component called WithAuth that "protects" the /revisited route from unauthorized access. There is no authentication developed. Just the HOC structure.



* Although Webpack is configured, it came with create-react-app and was not ejected. 



##Next steps

### Architecture improvements
* Routing history should be linked with the App store (Maybe use react-router-redux)
* Localization should be also stored in the App store
* Use CSS modules to truly have component style isolation. Eject create-react-app and configure Webpack



### App Improvements
* Change users list to make the header fixed
* Update countries names when language changes
* Set locale on MomentJs
* Make it responsive?