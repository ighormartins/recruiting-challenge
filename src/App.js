import React from 'react'
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'
import {connect} from 'react-redux'

import AppHeader from './Components/AppHeader'
import UsersViewContainer from './containers/UsersView/UsersViewContainer'
import RevisitedViewContainer from './containers/RevisitedView/RevisitedViewContainer'
import {COUNTRIES_ACTIONS} from './actions'
import WithAuth from './Components/WithAuth'
import './translations'
import './App.css'

class App extends React.Component {

    componentDidMount() {
        //Load one time fetch data
        this.props.onRequestCountries()
    }

    render() {
        return (
            <Router>
                <div id='viewport'>
                    <AppHeader/>
                    <Route exact path='/' component={UsersViewContainer}/>
                    <Route path='/revisited' component={WithAuth(RevisitedViewContainer)}/>
                </div>
            </Router>
        )
    }
}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        onRequestCountries: () => dispatch({type: COUNTRIES_ACTIONS.GET_COUNTRIES_REQUESTING}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)