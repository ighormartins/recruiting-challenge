import {combineReducers} from 'redux'
import {SELECT_USER, COUNTRIES_ACTIONS, USERS_ACTIONS} from '../actions'

function selectedUser(state = null, action) {
    switch (action.type) {
        case SELECT_USER:
            return action.user
        case USERS_ACTIONS.POST_USER_REQUESTING:
            return null
        case USERS_ACTIONS.GET_USERS_REQUESTING:
            return null
        case USERS_ACTIONS.POST_USER_SUCCESS:
            return action.user
        default:
            return state
    }
}

function countries(state = {
    isFetching: false,
    items: []
}, action) {
    switch (action.type) {
        case COUNTRIES_ACTIONS.GET_COUNTRIES_REQUESTING:
            return {...state, isFetching: true}
        case COUNTRIES_ACTIONS.GET_COUNTRIES_SUCCESS:
            return {
                ...state,
                isFetching: false,
                items: action.countries
            }
        case COUNTRIES_ACTIONS.GET_COUNTRIES_ERROR:
            return {
                ...state,
                isFetching: false,
                items: [],
                errors: action.error
            }
        default:
            return state
    }
}


function users(state = {
    isFetching: false,
    isSaving: false,
    items: []
}, action) {
    switch (action.type) {
        case USERS_ACTIONS.GET_USERS_REQUESTING:
            return {...state, isFetching: true}
        case USERS_ACTIONS.GET_USERS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                items: action.users
            }
        case USERS_ACTIONS.GET_USERS_ERROR:
            return {
                ...state,
                isFetching: false,
                items: [],
                errors: action.error
            }
        case USERS_ACTIONS.POST_USER_REQUESTING:
            return {
                ...state,
                isSaving: true
            }
        case USERS_ACTIONS.POST_USER_SUCCESS:
            return {
                ...state,
                isSaving: false,
                items: [...state.items, action.user]
            }
        default:
            return state
    }
}

function sessionUsers(state = [], action) {
    switch (action.type) {
        case USERS_ACTIONS.POST_USER_SUCCESS:
            return [...state, action.user]
        default:
            return state
    }
}


const rootReducer = combineReducers({
    countries,
    selectedUser,
    users,
    sessionUsers
})

export default rootReducer