import {I18n} from 'react-i18nify'

I18n.setTranslations({
    en: {
        application: {
            title: 'Recruiting Challenge'
        },
        form: {
            Name: 'Name',
            Surname: 'Surname',
            Country: 'Country',
            Birthday: 'Birthday'
        },
        Save: 'Save',
        UsersList: {
            NoUsersYet: 'There is no users yet',
            AddFirst: 'Add the first one'
        },
        Alert: {
            title: 'Hello %{name} %{surname} from %{country}',
            subtitle: 'On %{day} of %{month} you will have %{age} years'
        },
        SeeOldEntries: 'See old entries',
        backBtn: 'Go back'
    },
    pt: {
        application: {
            title: 'Desafio de contratação'
        },
        form: {
            Name: 'Nome',
            Surname: 'Apelido',
            Country: 'País',
            Birthday: 'Data de nascimento'
        },
        Save: 'Guardar',
        UsersList: {
            NoUsersYet: 'Ainda não existem utilizadores',
            AddFirst: 'Adiciona o primeiro'
        },
        Alert: {
            title: 'Olá %{name} %{surname} do %{country}',
            subtitle: 'Fazes %{age} anos no dia %{day} de %{month}'
        },
        SeeOldEntries: 'Ver entradas antigas',
        backBtn: 'Voltar'
    }
})

I18n.setLocale('en')