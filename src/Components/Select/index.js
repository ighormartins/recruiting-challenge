import React from 'react'
import PropTypes from 'prop-types'
import Option from 'muicss/lib/react/option'
import MUISelect from 'muicss/lib/react/select'


const Select = ({value, options, ...props}) => (
    <MUISelect name='input' value={value} {...props}>
        {options.map(option => <Option key={option.value} {...option}/>)}
    </MUISelect>
)

Select.propTypes = {
    value: PropTypes.any.isRequired,
    options: PropTypes.array.isRequired
}

export default Select