import React from 'react'

import Loading from '../Loading'


export default (Component) => class WithAuth extends React.Component {
    constructor() {
        super()
        this.state = {
            waiting: true,
        }
    }

    componentDidMount() {
        // Here we should check if there is a valid auth key token
        // Set waiting to false if it's ok or redirect user with React Router if no valid auth key was found
        this.setState({waiting: false})
    }

    render() {
        if (this.state.waiting) {
            return <Loading/>
        }


        // It's also possible inject an isAuthed prop to the component if we want to mount it anyway
        // but with a different behavior instead of redirecting
        return <Component {...this.props} />

    }

}