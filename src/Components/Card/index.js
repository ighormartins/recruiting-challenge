import React from 'react'
import classNames from 'classnames'
import Panel from 'muicss/lib/react/panel'

import './Card.css'


const Card = ({className, ...props}) => (
    <Panel className={classNames('card-style', className)} {...props}>
        {props.children}
    </Panel>
)

export default Card