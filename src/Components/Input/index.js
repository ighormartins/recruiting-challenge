import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import MUIInput from 'muicss/lib/react/input'

import './Input.css'


const Input = ({className, type, value, floatingLabel, ...props}) => (
    <MUIInput
        type={type}
        value={value}
        floatingLabel={floatingLabel}
        className={classNames('input-default-style', className)}
        {...props}
    />
)

Input.defaultProps = {
    type: 'text',
    floatingLabel: true
}

Input.propTypes = {
    type: PropTypes.oneOf(['text', 'date']),
    value: PropTypes.any.isRequired
}

export default Input
