import React from 'react'

import './EmptyState.css'


const EmptyState = ({className, title, subtitle, ...props}) => (
    <div className={['empty-state-style', className].join(' ')} {...props}>
        <h1 className='mui--text-dark-hint mui--text-center'>{title}</h1>
        {subtitle &&
        <h3 className='mui--text-dark-hint mui--text-center'>{subtitle}</h3>
        }
    </div>
)

export default EmptyState