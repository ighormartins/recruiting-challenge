import React from 'react'

import Card from '../Card'
import classNames from 'classnames'

const Alert = ({className, ...props}) => (
    <Card className={classNames('alert-card', className)} {...props}>
        {props.children}
    </Card>
)

export default Alert