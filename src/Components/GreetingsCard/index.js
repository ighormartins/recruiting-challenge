import React from 'react'
import moment from 'moment/moment'
import {Translate} from 'react-i18nify'

import Alert from '../Alert'
import EmptyState from '../EmptyState'
import "./GreetingsCard.css"


const GreetingsCard = ({name, surname, country, birthday}) => {
    const userBirthday = moment(birthday)
    const expectedAge = moment().diff(userBirthday, 'years') + 1

    const title = <Translate value='Alert.title' name={name} surname={surname} country={country}/>
    const subtitle = <Translate value='Alert.subtitle' day={userBirthday.format('D')}
                                month={userBirthday.format('MMMM')} age={expectedAge}/>

    return (
        <Alert className='user-greeting'>
            <EmptyState title={title} subtitle={subtitle}/>
        </Alert>
    )
}

export default GreetingsCard