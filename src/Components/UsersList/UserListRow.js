import React from 'react'
import {PropTypes} from 'prop-types'
import classNames from 'classnames'

import {Td, Tr} from '../Table/index'


const UsersListRow = ({name, surname, country, birthday, isSelected, className, ...props}) => (
    <Tr {...props} className={classNames('user-list-row', {'is-selected': isSelected}, className)}>
        <Td align='left'>{name} {surname}</Td>
        <Td align='right'>{country}</Td>
        <Td align='right'>{birthday}</Td>
    </Tr>
)


UsersListRow.propTypes = {
    name: PropTypes.string.isRequired,
    surname: PropTypes.string.isRequired,
    birthday: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    isSelected: PropTypes.bool
}

export default UsersListRow
