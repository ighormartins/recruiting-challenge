import React from 'react'
import {connect} from 'react-redux'
import {Translate} from 'react-i18nify'

import UsersListRow from './UserListRow'
import Table, {TBody, Th, THead, Tr} from '../../Components/Table'
import EmptyState from '../../Components/EmptyState'
import {SELECT_USER} from '../../actions'
import './UsersList.css'


class UsersList extends React.Component {
    handleUserRowClick = (user) => {
        this.props.onSelectUser(user)
    }

    render() {
        const {users, selectedUser} = this.props
        if (users.length === 0) {
            return (
                <EmptyState className='users-list-empty-state' title={<Translate value='UsersList.NoUsersYet'/>}
                            subtitle={<Translate value='UsersList.AddFirst'/>}/>
            )
        }

        return (
            <Table className='users-list'>
                <THead>
                <Tr>
                    <Th align='left'><Translate value='form.Name'/></Th>
                    <Th align='right'><Translate value='form.Country'/></Th>
                    <Th align='right'><Translate value='form.Birthday'/></Th>
                </Tr>
                </THead>
                <TBody>
                {users.map(user => (
                    <UsersListRow
                        isSelected={selectedUser && selectedUser.id === user.id}
                        onClick={this.handleUserRowClick.bind(this, user)} {...user}
                        key={user.id}
                    />)
                )}
                </TBody>
            </Table>
        )
    }
}


const mapStateToProps = state => {
    return {
        selectedUser: state.selectedUser
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSelectUser: (user) => dispatch({type: SELECT_USER, user: user})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList)

