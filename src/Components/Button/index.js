import React from 'react'
import PropTypes from 'prop-types'
import MUIButton from 'muicss/lib/react/button'

const Button = (props) => (
    <MUIButton {...props}>
        {props.children}
    </MUIButton>
)

Button.defaultProps = {
    color: 'primary'
}

Button.propTypes = {
    color: PropTypes.string
}

export default Button