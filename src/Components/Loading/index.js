import React from 'react'
import {BeatLoader} from 'react-spinners'


const Loading = (props) => (
    <BeatLoader {...props}/>
)

Loading.defaultProps = {
    color: 'white',
    size: 8,
}


export default Loading
