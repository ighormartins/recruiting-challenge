import React from 'react'
import {I18n} from 'react-i18nify'
import classNames from 'classnames'
import Container from 'muicss/lib/react/container'

import Button from '../Button'
import './AppHeader.css'


class AppHeader extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locale: 'en'
        }

        this.availableLocales = ['en', 'pt']
    }

    handleLocaleChange = (locale) => {
        this.setState({
            locale: locale
        })
        I18n.setLocale(locale)
    }

    render() {
        return (
            <div className='app-header mui--bg-primary'>
                <Container className='header-container'>
                    <div>
                        <h1>Ighor Martins</h1>
                        <h3>{I18n.t('application.title')}</h3>
                    </div>
                    <span className='flex'/>
                    <div className='lang-selector'>
                        {this.availableLocales.map(locale => (
                            <Button variant='flat' color='' size='small' key={locale}
                                    onClick={this.handleLocaleChange.bind(this, locale)}
                                    className={classNames({active: this.state.locale === locale})}
                            >{locale}</Button>
                        ))}
                    </div>
                </Container>
            </div>
        )
    }
}

export default AppHeader