import React from 'react'

import './Table.css'


export const Table = ({className, ...props}) => (
    <table className={['mui-table', className].join(' ')} {...props}>
        {props.children}
    </table>
)

export const THead = (props) => (
    <thead {...props}>
    {props.children}
    </thead>
)

export const TBody = (props) => (
    <tbody {...props}>
    {props.children}
    </tbody>
)


export const Tr = (props) => (
    <tr {...props}>
        {props.children}
    </tr>
)


export const Td = (props) => (
    <td {...props}>
        {props.children}
    </td>
)


export const Th = (props) => (
    <th {...props}>
        {props.children}
    </th>
)


export default Table