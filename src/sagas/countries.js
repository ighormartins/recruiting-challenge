import {call, put} from 'redux-saga/effects'
import axios from 'axios/index'

import {COUNTRIES_ACTIONS} from '../actions'


function fetchCountries() {
    return axios({
        method: 'get',
        url: 'https://restcountries.eu/rest/v2/all'
    })
}


export function* getCountriesWorkerSaga() {
    try {
        const response = yield call(fetchCountries)
        const countries = response.data
        yield put({type: COUNTRIES_ACTIONS.GET_COUNTRIES_SUCCESS, countries})
    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({type: COUNTRIES_ACTIONS.GET_COUNTRIES_ERROR, error})
    }
}