import {takeLatest, takeEvery} from 'redux-saga/effects'

import {COUNTRIES_ACTIONS, USERS_ACTIONS} from '../actions'
import {getCountriesWorkerSaga} from './countries'
import {getUsersWorkerSaga, saveUserWorkerSaga} from './users'


export function* watcherSaga() {
    yield takeLatest(COUNTRIES_ACTIONS.GET_COUNTRIES_REQUESTING, getCountriesWorkerSaga)
    yield takeLatest(USERS_ACTIONS.GET_USERS_REQUESTING, getUsersWorkerSaga)
    yield takeEvery(USERS_ACTIONS.POST_USER_REQUESTING, saveUserWorkerSaga)
}


