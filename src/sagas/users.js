import {call, put} from 'redux-saga/effects'

import {USERS_ACTIONS} from '../actions'


function fetchUsers() {
    return new Promise(
        function (resolve, reject) {
            try {
                const users = JSON.parse(localStorage.getItem('users')) || []
                window.setTimeout(() => resolve(users), 2000)
            } catch (e) {
                reject(e)
            }
        }
    )
}

function saveUser(data) {
    return new Promise(
        function (resolve, reject) {
            try {
                let users = JSON.parse(localStorage.getItem('users')) || []
                users.push(data)
                localStorage.setItem('users', JSON.stringify(users))
                window.setTimeout(() => resolve(data), 2000)
            } catch (e) {
                reject(e)
            }
        }
    )
}


export function* getUsersWorkerSaga() {
    try {
        const users = yield call(fetchUsers)
        yield put({type: USERS_ACTIONS.GET_USERS_SUCCESS, users})
    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({type: USERS_ACTIONS.GET_USERS_ERROR, error})
    }
}

export function* saveUserWorkerSaga(action) {
    try {
        const response = yield call(saveUser, action.data)
        yield put({type: USERS_ACTIONS.POST_USER_SUCCESS, user: response})
    } catch (error) {
        // dispatch a failure action to the store with the error
        //yield put({type: COUNTRIES_ACTIONS.GET_COUNTRIES_ERROR, error})
    }
}