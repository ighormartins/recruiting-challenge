export const COUNTRIES_ACTIONS = {
    GET_COUNTRIES_REQUESTING: 'GET_COUNTRIES_REQUESTING',
    GET_COUNTRIES_SUCCESS: 'GET_COUNTRIES_SUCCESS',
    GET_COUNTRIES_ERROR: 'GET_COUNTRIES_ERROR'
}

export const USERS_ACTIONS = {
    GET_USERS_REQUESTING: 'GET_USERS_REQUESTING',
    GET_USERS_SUCCESS: 'GET_USERS_SUCCESS',
    GET_USERS_ERROR: 'GET_USERS_ERROR',
    POST_USER_REQUESTING: 'POST_USER_REQUESTING',
    POST_USER_SUCCESS: 'POST_USER_SUCCESS'
}

export const SELECT_USER = 'SELECT_USER'
export const ADD_USER = 'ADD_USER'
