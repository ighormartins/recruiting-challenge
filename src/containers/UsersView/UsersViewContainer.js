import React from 'react'
import {connect} from 'react-redux'

import UsersView from './UsersView'
import Loading from '../../Components/Loading'


class UsersViewContainer extends React.Component {
    render() {
        const {fetchingCountries, fetchingUsers, countriesList, usersList, countriesError, usersError, selectedUser} = this.props

        if (fetchingCountries || fetchingUsers) {
            return (
                <h1 className='mui--text-dark-hint mui--text-center'>
                    <Loading color={'#ccc'} size={15}/><br/>
                    Loading App data
                </h1>
            )
        }

        if (countriesError || usersError) {
            return (
                <div>Oops. There was an error loading the App data</div>
            )
        }

        return (
            <UsersView countriesList={countriesList} usersList={usersList} selectedUser={selectedUser}/>
        )
    }
}


const mapStateToProps = state => {
    return {
        fetchingCountries: state.countries.isFetching,
        countriesList: state.countries.items,
        countriesError: state.countries.errors,
        usersList: state.sessionUsers,
        selectedUser: state.selectedUser
    }
}

const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersViewContainer)