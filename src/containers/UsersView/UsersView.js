import React from 'react'
import {PropTypes} from 'prop-types'
import Container from 'muicss/lib/react/container'
import Row from 'muicss/lib/react/row'
import Col from 'muicss/lib/react/col'
import {Translate} from 'react-i18nify'

import NewUserForm from './NewUserForm/NewUserForm'
import UsersList from '../../Components/UsersList/UsersList'
import Card from '../../Components/Card'
import Link from 'react-router-dom/es/Link'
import GreetingsCard from '../../Components/GreetingsCard'
import './UsersView.css'

class UsersView extends React.Component {
    render() {
        const {countriesList, usersList, selectedUser} = this.props

        return (
            <Container id='users-view' className='page-content'>
                <Row className='column flex'>
                    <Col md={5}>
                        <Card className='new-user-form-wrapper'>
                            <NewUserForm countriesList={countriesList}/>
                        </Card>
                        {selectedUser && <GreetingsCard {...this.props.selectedUser}/>}
                    </Col>
                    <Col md={7} className='mui--text-right'>
                        <Card className='users-list-wrapper'>
                            <UsersList users={usersList}/>
                        </Card>
                        <Link to='/revisited'>
                            <Translate value='SeeOldEntries'/>
                        </Link>
                    </Col>
                </Row>
            </Container>
        )
    }
}


UsersView.propTypes = {
    countriesList: PropTypes.array.isRequired
}


export default UsersView



