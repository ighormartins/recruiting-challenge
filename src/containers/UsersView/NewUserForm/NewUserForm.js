import React from 'react'
import {PropTypes} from 'prop-types'
import {connect} from 'react-redux'

import Input from '../../../Components/Input'
import Select from '../../../Components/Select'
import Button from '../../../Components/Button'
import {Translate} from 'react-i18nify'
import {USERS_ACTIONS} from '../../../actions'
import Loading from '../../../Components/Loading'
import './NewUserForm.css'


class NewUserForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            surname: '',
            birthday: '',
            country: 'Portugal'
        }
    }

    handleFormSubmit = (e) => {
        e.preventDefault()
        const newUser = {
            id: Math.random().toString(36).substr(2, 16),
            ...this.state
        }

        this.props.onSaveUser(newUser)
    }

    handleInputChange = (event) => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    render() {
        const {countriesList, isSavingUser} = this.props
        const countriesListArr = countriesList.map(country => ({value: country.name, label: country.name}))

        const tname = <Translate value='form.Name'/>
        const tsurname = <Translate value='form.Surname'/>
        const tbirthday = <Translate value='form.Birthday'/>
        const tcountry = <Translate value='form.Country'/>

        return (
            <form className='new-user-form' onSubmit={this.handleFormSubmit}>
                <Input required value={this.state.name} label={tname} name='name' onChange={this.handleInputChange}
                       disabled={isSavingUser}/>
                <Input required value={this.state.surname} label={tsurname} name='surname'
                       onChange={this.handleInputChange} disabled={isSavingUser}/>
                <Select name='country' value={this.state.country} label={tcountry}
                        options={countriesListArr} onChange={this.handleInputChange}
                        disabled={isSavingUser}
                />
                <Input required type='date' value={this.state.birthday} label={tbirthday} name='birthday'
                       onChange={this.handleInputChange} floatingLabel={false} disabled={isSavingUser}/>

                <div className='mui--text-right'>
                    <Button disabled={isSavingUser} color='accent'>
                        {!isSavingUser && <Translate value='Save'/>}
                        <Loading loading={isSavingUser}/>
                    </Button>
                </div>
            </form>
        )
    }
}


NewUserForm.propTypes = {
    countriesList: PropTypes.array.isRequired
}

const mapStateToProps = state => {
    return {
        isSavingUser: state.users.isSaving
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSaveUser: (data) => dispatch({type: USERS_ACTIONS.POST_USER_REQUESTING, data: data})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewUserForm)
