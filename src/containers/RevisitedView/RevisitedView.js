import React from 'react'
import {PropTypes} from 'prop-types'
import Container from 'muicss/lib/react/container'
import Row from 'muicss/lib/react/row'
import Col from 'muicss/lib/react/col'
import {Translate} from 'react-i18nify'

import Card from '../../Components/Card'
import './RevisitedView.css'
import Link from 'react-router-dom/es/Link'
import Loading from '../../Components/Loading'
import UsersList from '../../Components/UsersList/UsersList'
import GreetingsCard from '../../Components/GreetingsCard'
import Button from '../../Components/Button'

class RevisitedView extends React.Component {
    render() {
        const {usersList, fetchingUsers, selectedUser} = this.props

        return (
            <Container id='revisited-view' className='page-content'>
                <Row className='column flex'>
                    <Col md={5}>
                        <Link to='/'>
                            <Button color='' variant='flat' size='large'>
                                &larr;
                                &nbsp; &nbsp;
                                <Translate value='backBtn'/>
                            </Button>
                        </Link>
                        {selectedUser && <GreetingsCard {...this.props.selectedUser}/>}
                    </Col>
                    <Col md={7} className='mui--text-right'>
                        <Card className='users-list-wrapper mui--text-center'>
                            <div className='loading-wrapper'><Loading color='#222' loading={fetchingUsers}/></div>
                            {!fetchingUsers && <UsersList users={usersList}/>}
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}


RevisitedView.propTypes = {
    usersList: PropTypes.array.isRequired,
    fetchingUsers: PropTypes.bool.isRequired
}


export default RevisitedView



