import React from 'react'
import {connect} from 'react-redux'

import {USERS_ACTIONS} from '../../actions'
import RevisitedView from './RevisitedView'


class RevisitedViewContainer extends React.Component {
    componentWillMount() {
        this.props.onRequestUsers()
    }

    render() {
        const {fetchingUsers, usersList, selectedUser} = this.props

        return (
            <RevisitedView usersList={usersList} fetchingUsers={fetchingUsers} selectedUser={selectedUser}/>
        )
    }
}


const mapStateToProps = state => {
    return {
        fetchingUsers: state.users.isFetching,
        usersError: state.users.errors,
        usersList: state.users.items,
        selectedUser: state.selectedUser
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRequestUsers: () => dispatch({type: USERS_ACTIONS.GET_USERS_REQUESTING}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RevisitedViewContainer)